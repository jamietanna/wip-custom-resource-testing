require 'spec_helper'
load_resource('cr', 'echo')
load_provider('cr', 'echo')

describe Chef::Provider::CrEcho do
  let(:resource_name) { 'echo' }
  let(:resource_class) { Chef::Resource::CrEcho }

  it 'is whyrun support' do
    expect(provider).to be_whyrun_supported
  end

  it 'for :create action, it exists' do
    expect(provider.run_action(:create))
    expect(runner_resources).to include('file[write echo to a file]')
  end
end
