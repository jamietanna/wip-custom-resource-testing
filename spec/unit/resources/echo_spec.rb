require 'spec_helper'
load_resource('cr', 'echo')

describe Chef::Resource::CrEcho do
  let(:resource_name) { 'echo' }

  it 'sets the default attribute to name' do
    expect(resource.name).to eq('echo')
  end

  it 'action defaults to :create' do
    expect(resource.action).to eq([:create])
  end
end
