def whyrun_supported?
  true
end

use_inline_resources

action :create do
  file 'write echo to a file' do
    path '/tmp/hello.txt'
    content 'echo'
  end
end
